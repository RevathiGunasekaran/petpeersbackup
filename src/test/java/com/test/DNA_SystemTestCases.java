package com.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.main.MainAppController;
import com.model.Pets;
import com.model.User;
import com.service.UserService;
import com.service.UserServiceImpl;

//@SpringBootTest
//@AutoConfigureMockMvc
@RunWith(MockitoJUnitRunner.class)
public class DNA_SystemTestCases {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private UserDao userRespository;
	@Autowired
	private UserService userService;

	@Mock
	private UserDaoImpl userRespositoryImpl;
	@Mock
	private UserServiceImpl userServiceImpl;
	
	@Mock
	private SessionFactory mockedSessionFactory;
	@Mock
	private Session mockedSession;
	@Mock
	private Transaction mockedTransaction;
	
	@Mock
	private Query mockedQuery;

	@Before
	public void setup() {
		mockedSessionFactory = mock(SessionFactory.class);
		mockedSession = mock(Session.class);
		mockedTransaction = mock(Transaction.class);
		lenient().when(mockedSessionFactory.openSession()).thenReturn(mockedSession);
		lenient().when(mockedSession.beginTransaction()).thenReturn(mockedTransaction);
		lenient().doCallRealMethod().when(userRespositoryImpl).setSessionFactory(mockedSessionFactory);
		Mockito.mockitoSession().initMocks(this).strictness(Strictness.WARN);
		userRespository = mock(UserDao.class);
		userService = mock(UserService.class);
		this.mockMvc = MockMvcBuilders.standaloneSetup(new MainAppController(userService)).build();
		MockitoAnnotations.initMocks(this);
		
		userRespositoryImpl.setSessionFactory(mockedSessionFactory);

		// setup view model with dependencies mocked

	}

	@Test
	public void verifyUserInsert() {
		User usr = new User("$admin", "$admin", "$admin");
		usr.getPets();
		usr.setPets(new ArrayList<>());
		mockedSessionFactory = mock(SessionFactory.class);
		mockedSession = mock(Session.class);
		mockedTransaction = mock(Transaction.class);
		lenient().when(mockedSessionFactory.openSession()).thenReturn(mockedSession);
		lenient().when(mockedSession.beginTransaction()).thenReturn(mockedTransaction);
		
		UserDaoImpl userRepoLocal = mock(UserDaoImpl.class);
		 //doThrow(RuntimeException.class).when(userRepoLocal).setSessionFactory(any());//.thenReturn(mockedSessionFactory);
		 doCallRealMethod().when(userRepoLocal).setSessionFactory(mockedSessionFactory);
		userRepoLocal.setSessionFactory(mockedSessionFactory);
		UserServiceImpl usrImpl4 = new UserServiceImpl(userRepoLocal);
		usrImpl4.setUserRepo(new UserDaoImpl());
		UserServiceImpl usrImpl = mock(UserServiceImpl.class);
		usrImpl.setUserRepo(mock(UserDaoImpl.class));
		doCallRealMethod().when(usrImpl).saveUser(usr);
		doCallRealMethod().when(userRepoLocal).saveUser(usr);
		try {
			usrImpl.saveUser(usr);
		} catch (Exception e) {
		}
		try {
			userRepoLocal.saveUser(usr);
		} catch (Exception e) {
		}

		doCallRealMethod().when(userRespositoryImpl).saveUser(usr);// .thenCallRealMethod();
		// userServiceImpl.setUserRepo(userRespositoryImpl);
		// doCallRealMethod().when(userServiceImpl).saveUser(usr);
		userRespositoryImpl.saveUser(usr);
		// userServiceImpl.saveUser(usr);

	}

	@Test
	public void testUserInsertFailure() throws Exception {
		User usr = new User("dddddd22222222222222222222ddddfdfdfdfdfddddddddddddddddddddddddddd",
				"dadfdfdddddddddddddddddddddddddddddddd", "dadfdfddd1222222222ddddddddddddddddddddddddddddd");
		List<User> usrList = new ArrayList<>();
		usrList.add(usr);
		doCallRealMethod().when(userRespositoryImpl).checkUser(any(), any());// .thenCallRealMethod();
		userRespositoryImpl.checkUser(usr.getUserName(), usr.getUserPasswd());
		UserServiceImpl usrImpl = new UserServiceImpl(mock(UserDaoImpl.class));
		usrImpl.checkUser(usr.getUserName(), usr.getUserPasswd());
		doCallRealMethod().when(userRespositoryImpl).home();// .thenCallRealMethod();
		userRespositoryImpl.home();
		usrImpl.home();
		doReturn(usr).when(userService).checkUser(any(), any());
		doReturn(usrList).when(userService).home();
		mockMvc.perform(post("/checkUser").param("userId", "0").param("userName", "\"" + usr.getUserName() + "\"")
				.param("userPasswd", "\"" + usr.getUserPasswd() + "\"")
				.param("confirmUserPasswd", "\"" + usr.getConfirmUserPasswd() + "\"")
				.contentType(MediaType.APPLICATION_JSON).with(new RequestPostProcessor() {
					@Override
					public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
						request.getSession().setAttribute("userId", "0");
						return request;
					}
				})).andExpect(view().name("HomePage"));

	}

	@Test
	public void testIndexPage() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isOk());
	}

	@Test
	public void testMyPets() throws Exception {
		// when(userService.myPetList(0)).thenCallRealMethod();
		doCallRealMethod().when(userRespositoryImpl).myPetList(0);// .thenCallRealMethod();
		userRespositoryImpl.myPetList(0);
		// UserServiceImpl usrImpl = new UserServiceImpl(new UserDaoImpl());

		try {
			UserServiceImpl usrImpl = (mock(UserServiceImpl.class));
			doCallRealMethod().when(usrImpl).myPetList(0);
			usrImpl.myPetList(0);
		} catch (Exception e) {
		}

		try {
			UserServiceImpl usrImpl2 = (mock(UserServiceImpl.class));
			doCallRealMethod().when(usrImpl2).home();
			usrImpl2.home();
		} catch (Exception e) {
		}

		doCallRealMethod().when(userRespositoryImpl).petList(0);// .thenCallRealMethod();
		userRespositoryImpl.petList(0);

		mockMvc.perform(get("/myPets").with(new RequestPostProcessor() {
			@Override
			public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
				request.getSession().setAttribute("userId", "0");
				return request;
			}
		})).andExpect(status().isOk());
	}

	@Test
	public void testLogout() throws Exception {
		User usr = new User("$admin", "$admin", "$admin");
		mockMvc.perform(get("/logout").param("userId", "0").param("userName", "\"" + usr.getUserName() + "\"")
				.param("userPasswd", "\"" + usr.getUserPasswd() + "\"")
				.param("confirmUserPasswd", "\"" + usr.getConfirmUserPasswd() + "\"")).andExpect(status().isOk());
	}

	@Test
	public void testLoginPage() throws Exception {
		mockMvc.perform(get("/Login")).andExpect(status().isOk());
	}

	@Test
	public void testRegisterPage() throws Exception {
		// when(userService.saveUser(new User(0,"A","A","A"))).thenCallRealMethod();
		User usr = new User("$admin", "$admin", "$admin");
		mockMvc.perform(get("/Register").param("userId", "0").param("userName", "\"" + usr.getUserName() + "\"")
				.param("userPasswd", "\"" + usr.getUserPasswd() + "\"")
				.param("confirmUserPasswd", "\"" + usr.getConfirmUserPasswd() + "\"")).andExpect(status().isOk());
	}

	@Test
	public void testHomePage() throws Exception {
		// when(userService.home()).thenCallRealMethod();
		doCallRealMethod().when(userRespositoryImpl).home();// .thenCallRealMethod();
		userRespositoryImpl.home();

		User usr = new User("$admin", "$admin", "$admin");
		mockMvc.perform(get("/home").param("userId", "0").param("userName", "\"" + usr.getUserName() + "\"")
				.param("userPasswd", "\"" + usr.getUserPasswd() + "\"")
				.param("confirmUserPasswd", "\"" + usr.getConfirmUserPasswd() + "\"")).andExpect(status().isOk());

	}

	@Test
	public void testSavePet() throws Exception {

		mockMvc.perform(
				post("/savePet")
				.param("petId", "1")
				.param("petName", "A")
				.param("petAge", "10")
				.param("petPlace", "CH"));
		doCallRealMethod().when(userRespositoryImpl).savePet(any());// .thenCallRealMethod();
		userRespositoryImpl.savePet(new Pets(1, "A", 1, "CH"));

		// UserServiceImpl usrImpl = new UserServiceImpl(mock(UserDaoImpl.class));
		UserServiceImpl usrImpl = (mock(UserServiceImpl.class));
		lenient().doCallRealMethod().when(usrImpl).savePet(new Pets(1, "A", 1, "CH"));
		usrImpl.savePet(new Pets(1, "A", 1, "CH"));

	}

	@Test
	public void testValidSavePet() throws Exception {

		mockedSessionFactory = mock(SessionFactory.class);
		mockedSession = mock(Session.class);
		mockedTransaction = mock(Transaction.class);
		lenient().when(mockedSessionFactory.openSession()).thenReturn(mockedSession);
		lenient().when(mockedSession.beginTransaction()).thenReturn(mockedTransaction);
		
		UserDaoImpl userRepoLocal = mock(UserDaoImpl.class);
		 //doThrow(RuntimeException.class).when(userRepoLocal).setSessionFactory(any());//.thenReturn(mockedSessionFactory);
		lenient().doCallRealMethod().when(userRepoLocal).setSessionFactory(mockedSessionFactory);
		userRepoLocal.setSessionFactory(mockedSessionFactory);
		mockMvc.perform(
				post("/savePet")
				.param("petId", "1")
				.param("petName", "A")
				.param("petAge", "10")
				.param("petPlace", "CH"));
		doCallRealMethod().when(userRepoLocal).savePet(any());// .thenCallRealMethod();
		userRepoLocal.savePet(new Pets(1, "A", 1, "CH"));

		// UserServiceImpl usrImpl = new UserServiceImpl(mock(UserDaoImpl.class));
		UserServiceImpl usrImpl = (mock(UserServiceImpl.class));
		lenient().doCallRealMethod().when(usrImpl).savePet(new Pets(1, "A", 1, "CH"));
		usrImpl.savePet(new Pets(1, "A", 1, "CH"));

	}
	@Test
	public void testAddPet() throws Exception {
		Pets pet = new Pets(1, "A", 1, "CH");
		doCallRealMethod().when(userRespositoryImpl).savePet(any());// .thenCallRealMethod();
		userRespositoryImpl.savePet(pet);

		try {
			UserServiceImpl usrImpl2 = mock(UserServiceImpl.class);
			usrImpl2.setUserRepo(mock(UserDaoImpl.class));
			lenient().doCallRealMethod().when(usrImpl2).savePet(pet);
			usrImpl2.savePet(new Pets(1, "A", 1, "CH"));
		} catch (Exception e) {
		}
		mockMvc.perform(get("/addPet")).andExpect(status().isOk());
	}

	@Test
	public void testUsertReadFailure() throws Exception {
		User usr = new User("", "", "");
		mockMvc.perform(post("/checkUser").param("userId", "0").param("userName", "\"" + usr.getUserName() + "\"")
				.param("userPasswd", "\"" + usr.getUserPasswd() + "\"")
				.param("confirmUserPasswd", "\"" + usr.getConfirmUserPasswd() + "\"").with(new RequestPostProcessor() {
					@Override
					public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
						request.getSession().setAttribute("userId", "0");
						return request;
					}
				}).contentType(MediaType.APPLICATION_JSON)).andExpect(view().name("LoginPage"));
	}

	@Test
	public void testPetInsertValidation() throws Exception {
		Pets pet = new Pets();
		pet.setPetId(1);
		pet.setPetName("dddddddddddddddddddddddddddddd");
		pet.setPetAge(10);
		pet.setPetPlace("CH");
		pet.setUser(new User(0, "A", "A", "A"));
		pet.getUser();
		pet.getPetId();
		pet.getPetName();
		pet.getPetPlace();
		pet.getPetAge();
		pet.setUser(new User());
		pet.getUser();
		userService.savePet(pet);
		userRespository.savePet(pet);		
	}

	@Test
	public void testInsertFailure() throws Exception {
		try {
			userRespository.savePet(null);
			userService.savePet(null);
			assertTrue(true);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void testBuyPet() throws Exception {
		UserDaoImpl usrRepoLocal = mock(UserDaoImpl.class);
		doCallRealMethod().when(usrRepoLocal).buyPet(0, 0);
		usrRepoLocal.buyPet(0, 0);
		doCallRealMethod().when(userRespositoryImpl).buyPet(0, 0);// .thenCallRealMethod();
		userRespositoryImpl.buyPet(0, 0);
		UserServiceImpl usrImpl = (mock(UserServiceImpl.class));
		usrImpl.setUserRepo(mock(UserDaoImpl.class));
		doCallRealMethod().when(usrImpl).buyPet(0, 0);// .thenCallRealMethod();
		try {
			usrImpl.buyPet(0, 0);
		} catch (Exception e) {
		}
		mockMvc.perform(get("/buyPet").with(new RequestPostProcessor() {
			@Override
			public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
				request.getSession().setAttribute("userId", "0");
				request.setParameter("petId", "0");
				return request;
			}
		}).contentType(MediaType.APPLICATION_JSON)).andExpect(redirectedUrl("/myPets"));
	}
	
	@Test
	public void testValidBuyPet() throws Exception {
		mockedSessionFactory = mock(SessionFactory.class);
		mockedSession = mock(Session.class);
		mockedTransaction = mock(Transaction.class);
		mockedQuery = mock(Query.class);
		lenient().when(mockedSessionFactory.openSession()).thenReturn(mockedSession);
		lenient().when(mockedSession.beginTransaction()).thenReturn(mockedTransaction);
		lenient().when(mockedSession.createQuery("from User where id=" + 1)).thenReturn(mockedQuery);
		lenient().when(mockedQuery.uniqueResult()).thenReturn(Object.class);
		UserDaoImpl userRepoLocal = mock(UserDaoImpl.class);
		 //doThrow(RuntimeException.class).when(userRepoLocal).setSessionFactory(any());//.thenReturn(mockedSessionFactory);
		lenient().doCallRealMethod().when(userRepoLocal).setSessionFactory(mockedSessionFactory);
		userRepoLocal.setSessionFactory(mockedSessionFactory);
		doCallRealMethod().when(userRepoLocal).buyPet(0, 0);
		userRepoLocal.buyPet(0, 0);
		doCallRealMethod().when(userRespositoryImpl).buyPet(0, 0);// .thenCallRealMethod();
		userRespositoryImpl.buyPet(0, 0);
	}
	
	@Test
	public void testValidHome() throws Exception {
		mockedSessionFactory = mock(SessionFactory.class);
		mockedSession = mock(Session.class);
		mockedTransaction = mock(Transaction.class);
		Query<?> query = mock(Query.class);
		lenient().when(mockedSessionFactory.openSession()).thenReturn(mockedSession);
		lenient().when(mockedSession.beginTransaction()).thenReturn(mockedTransaction);
		lenient().when(mockedSession.createQuery("from Pets")).thenReturn(query);
		
		UserDaoImpl userRepoLocal = mock(UserDaoImpl.class);
		 //doThrow(RuntimeException.class).when(userRepoLocal).setSessionFactory(any());//.thenReturn(mockedSessionFactory);
		lenient().doCallRealMethod().when(userRepoLocal).setSessionFactory(mockedSessionFactory);
		userRepoLocal.setSessionFactory(mockedSessionFactory);
		doCallRealMethod().when(userRepoLocal).home();
		userRepoLocal.home();
	}
	
	@Test
	public void testValidPetList() throws Exception {
		mockedSessionFactory = mock(SessionFactory.class);
		mockedSession = mock(Session.class);
		mockedTransaction = mock(Transaction.class);
		Query<?> query = mock(Query.class);
		lenient().when(mockedSessionFactory.openSession()).thenReturn(mockedSession);
		lenient().when(mockedSession.beginTransaction()).thenReturn(mockedTransaction);
		lenient().when(mockedSession.createQuery("from Pets where petId=" + 1)).thenReturn(query);
		
		UserDaoImpl userRepoLocal = mock(UserDaoImpl.class);
		 //doThrow(RuntimeException.class).when(userRepoLocal).setSessionFactory(any());//.thenReturn(mockedSessionFactory);
		lenient().doCallRealMethod().when(userRepoLocal).setSessionFactory(mockedSessionFactory);
		userRepoLocal.setSessionFactory(mockedSessionFactory);
		doCallRealMethod().when(userRepoLocal).petList(1);
		userRepoLocal.petList(1);
		
	
	}
	
	@Test
	public void testValidMyPetList() throws Exception {
		mockedSessionFactory = mock(SessionFactory.class);
		mockedSession = mock(Session.class);
		mockedTransaction = mock(Transaction.class);
		Query<?> query = mock(Query.class);
		lenient().when(mockedSessionFactory.openSession()).thenReturn(mockedSession);
		lenient().when(mockedSession.beginTransaction()).thenReturn(mockedTransaction);
		lenient().when(mockedSession.createQuery("from Pets where petownerid=" + 1)).thenReturn(query);
		
		UserDaoImpl userRepoLocal = mock(UserDaoImpl.class);
		 //doThrow(RuntimeException.class).when(userRepoLocal).setSessionFactory(any());//.thenReturn(mockedSessionFactory);
		lenient().doCallRealMethod().when(userRepoLocal).setSessionFactory(mockedSessionFactory);
		userRepoLocal.setSessionFactory(mockedSessionFactory);
		doCallRealMethod().when(userRepoLocal).myPetList(1);
		userRepoLocal.myPetList(1);
	}
	
	@Test
	public void testValidCheckUsr() throws Exception {
		User usr = new User(1,"A","a","a");
		mockedSessionFactory = mock(SessionFactory.class);
		mockedSession = mock(Session.class);
		mockedTransaction = mock(Transaction.class);
		mockedQuery = mock(Query.class);
		lenient().when(mockedSessionFactory.openSession()).thenReturn(mockedSession);
		lenient().when(mockedSession.beginTransaction()).thenReturn(mockedTransaction);
		
		String name="Admin";
		String passwd="Admin";
		lenient().when(mockedSession.createQuery("from User where userName='" + name + "' and userPasswd='" + passwd + "'")).thenReturn(mockedQuery);
		lenient().when(mockedQuery.uniqueResult()).thenReturn(User.class);
		UserDaoImpl userRepoLocal = mock(UserDaoImpl.class);
		 //doThrow(RuntimeException.class).when(userRepoLocal).setSessionFactory(any());//.thenReturn(mockedSessionFactory);
		lenient().doCallRealMethod().when(userRepoLocal).setSessionFactory(mockedSessionFactory);
		userRepoLocal.setSessionFactory(mockedSessionFactory);
		doCallRealMethod().when(userRepoLocal).myPetList(1);
		userRepoLocal.myPetList(1);
		mockMvc.perform(post("/save").param("userId", "0").param("userName", "\"" + usr.getUserName() + "\"")
				.param("userPasswd", "\"" + usr.getUserPasswd() + "\"")
				.param("confirmUserPasswd", "\"" + usr.getConfirmUserPasswd() + "\"").with(new RequestPostProcessor() {
					@Override
					public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
						request.setAttribute("userId", "1");
						return request;
					}
				}).contentType(MediaType.APPLICATION_JSON)).andExpect(redirectedUrl("/Login"));
	}

	@Test
	public void testUserRead() throws Exception {
		User usr = new User("$admin", "$admin", "$admin");
		List<User> usrList = new ArrayList<>();
		usrList.add(usr);
		UserServiceImpl usrImpl = (mock(UserServiceImpl.class));
		// UserServiceImpl usrImpl = new UserServiceImpl(mock(UserDaoImpl.class));
		lenient().doCallRealMethod().when(usrImpl).checkUser(any(), any());
		doReturn(usr).when(userService).checkUser(any(), any());
		doReturn(usrList).when(userService).home();
		mockMvc.perform(post("/save").param("userId", "0").param("userName", "\"" + usr.getUserName() + "\"")
				.param("userPasswd", "\"" + usr.getUserPasswd() + "\"")
				.param("confirmUserPasswd", "\"" + usr.getConfirmUserPasswd() + "\"")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(redirectedUrl("/Login"));
		mockMvc.perform(post("/checkUser").param("userId", "0").param("userName", "\"" + usr.getUserName() + "\"")
				.param("userPasswd", "\"" + usr.getUserPasswd() + "\"")
				.param("confirmUserPasswd", "\"" + usr.getConfirmUserPasswd() + "\"").with(new RequestPostProcessor() {
					@Override
					public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
						request.getSession().setAttribute("userId", "0");
						return request;
					}
				}).contentType(MediaType.APPLICATION_JSON)).andExpect(view().name("HomePage"));
	}

	@Test
	public void testUserInsert() throws Exception {
		User usr = new User("$admin$", "$admin$", "$admin$");
		doCallRealMethod().when(userRespositoryImpl).checkUser(usr.getUserName(), usr.getUserPasswd());// .thenCallRealMethod();
		userRespositoryImpl.checkUser(usr.getUserName(), usr.getUserPasswd());

		// UserServiceImpl usrImpl = new UserServiceImpl(mock(UserDaoImpl.class));
		UserServiceImpl usrImpl = mock(UserServiceImpl.class);
		usrImpl.setUserRepo(mock(UserDaoImpl.class));
		doCallRealMethod().when(usrImpl).checkUser(usr.getUserName(), usr.getUserPasswd());// .thenCallRealMethod();
		try {
			usrImpl.checkUser(usr.getUserName(), usr.getUserPasswd());
		} catch (Exception e) {
		}
		mockMvc.perform(post("/save").param("userId", "0").param("userName", "\"" + usr.getUserName() + "\"")
				.param("userPasswd", "\"" + usr.getUserPasswd() + "\"")
				.param("confirmUserPasswd", "\"" + usr.getConfirmUserPasswd() + "\"").with(new RequestPostProcessor() {
					@Override
					public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
						request.setAttribute("userId", "1");
						return request;
					}
				}).contentType(MediaType.APPLICATION_JSON)).andExpect(redirectedUrl("/Login"));
	}

	@Test
	public void testUserInsertValidation() throws Exception {
		User usr = new User("", "", "");
		// when(userService)
		mockMvc.perform(post("/save").param("userId", "0").param("userName", "\"" + usr.getUserName() + "\"")
				.param("userPasswd", "\"" + usr.getUserPasswd() + "\"")
				.param("confirmUserPasswd", "\"" + usr.getConfirmUserPasswd() + "\"")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(redirectedUrl("/Login"));
	}

	@Test
	public void testGetAll() throws Exception {
		try {
			userRespository.home();
			userService.home();
			assertTrue(true);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void testInsert() throws Exception {
		Pets pet = new Pets("Star", 11, "CH");
		try {
			userRespository.savePet(pet);
			userService.savePet(pet);
			assertTrue(true);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void testUpdate() throws Exception {
		Pets pet = new Pets("Star", 11, "CH");
		try {
			userRespository.savePet(pet);
			userService.savePet(pet);
			assertTrue(true);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void testReadFailure() throws Exception {
		mockMvc.perform(get("/pets/{id}", 999).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void testUpdateValidation() throws Exception {
		try {
			userRespository.savePet(null);
			userService.savePet(null);
			assertTrue(true);
		} catch (Exception e) {
			fail();
		}
	}

	public static String asJsonString(final Object obj) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
