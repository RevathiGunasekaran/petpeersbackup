package com.main;

/**
  
  Case Study: Pet Peers 
  
  Technology: Spring MVC, Spring, JSP (HTML, CSS, JS etc.), Servlet Filter, Hibernate ORM, Log4j 2
  
  Description: The task is develop a 'Pet Peers'  a web based application, which provide features such as  storing & retrieving pet information, listing available pets and view details for a specific pet. It also allows a user to login/register, buy pets and view the pets owned by him. 
   
  The following are the Use Cases for the Pet Peers application.
         1.	User Registration - New users can register themselves by entering a unique user name and password.
         2.	User Login - Users should be able to login into the application by entering a correct user name and password
         3.	View List of all Pets - This functionality lists all pets and also indicates whether a Pet is "Sold Out" or available for purchase. 
         	If available, it provides a link to buy that Pet.
         4.	Add a Pet - A user can enter a new Pet to the list (anyone can buy this pet).
         5.	Buy a Pet - If a User likes an available Pet, she / he can buy the same by clicking on the "Buy" button.
         6.	List of owned Pets - A logged in user can view the list of Pets owned by him/her by clicking on "My Pets" button.
         7.	User Logout - A user can logout of the application by clicking on "Logout" button. After logout, user can access the application functionalities only after logging in again.
         8. Please show both success and failure message for each use case scenarios in the relevant pages. 
	
	You are provided with a partially implemented eclipse Maven web application project template, herewith you are expected to create the necessary objects and components in the given project template. 

    Rules:
         1. During first time loading if there are error, please clean the project and update Maven Project with "Force Update of snapshot/Release" option
         2. Do not modify this Class or its methods name 
         2. Do not alter the request mapping URI, methods signature and JSP names. 
         3. You can add methods or classes to fulfill the requirements
         4. Follow Java coding conventions, best practices for better assessment feedback
         5. Write as many unit test cases in WebApplicationUnitTest.java for better test pass % and code coverage.
         6. Automatically check-in happens for every 30 minutes, for manual check-in refer Step 7a or 7b. 
         7. After completion of the use case development, please submit the code using one of below options
	
			a. Goto Git Staging window (in Eclipse -> Goto Window Menu -> Show View -> Others -> filter for Git Staging -> Click Open), add the necessary files to "Staged Changes" from "Unstaged Changes", enter the commit message 
			   then click "Commit and Push", click "Preview", click "Push" and finally click "Close" button
							or
			b. Goto Project Explorer (in Eclipse), right click on the UseCaseSubmission.bat and select open		
         8. After code submission please allow 2 minutes for coding analysis process to complete. 				
	
  	Deviating above rules will impact the assessment feedback.

 */

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.model.Pets;
import com.model.User;
import com.service.UserService;

@RestController
public class MainAppController {

	public MainAppController(UserService userService) {
		this.userService = userService;
	}

	@Autowired
	UserService userService;

	@GetMapping("/")
	public ModelAndView index() {
		return new ModelAndView("Index");
	}

	@GetMapping("/Login")
	public ModelAndView login(@ModelAttribute("user") User user) {
		return new ModelAndView("LoginPage");
	}

	@PostMapping("/checkUser")
	public ModelAndView checkUser(HttpServletRequest req, @ModelAttribute("user") User user) {
		return null;
	}

	@GetMapping("/Register")
	public ModelAndView register(@ModelAttribute("user") User user) {
		return new ModelAndView("Registration");
	}

	@PostMapping("/save")
	public ModelAndView saveUser(@ModelAttribute("user") User user) {
		return null;
	}

	@GetMapping("/home")
	public ModelAndView home(@ModelAttribute("user") User user) {
		return null;
	}

	@GetMapping("/buyPet")
	public ModelAndView buyPet(HttpServletRequest req) {
		return null;
	}

	@GetMapping("/addPet")
	public ModelAndView addpet() {
		return null;
	}

	@PostMapping("/savePet")
	public ModelAndView savepet(@ModelAttribute("pets") Pets pets) {
		return null;
	}

	@GetMapping("/myPets")
	public ModelAndView petList(HttpServletRequest req) {
		return null;
	}

	@GetMapping("/logout")
	public ModelAndView logout(@ModelAttribute("user") User user) {
		return new ModelAndView("LoginPage");
	}
}
