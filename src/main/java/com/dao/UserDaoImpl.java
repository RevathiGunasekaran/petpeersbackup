package com.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Pets;
import com.model.User;

@Repository
public class UserDaoImpl implements UserDao {
	@Autowired
	SessionFactory sessionFactory;

	public void saveUser(User user) {
	}

	public List<Pets> home() {
		return null;
	}

	public void savePet(Pets pets) {
	}

	public Pets petList(int id) {
		return null;
	}

	public List<Pets> myPetList(int userId) {
		return null;
	}

	public User checkUser(String name, String passwd) {
		return null;
	}

	@Override
	public void buyPet(int petId, int userId) {
	}

	public void setSessionFactory(SessionFactory mockedSessionFactory) {
		this.sessionFactory = mockedSessionFactory;		
	}
}
