package com.dao;

import java.util.List;

import com.model.Pets;
import com.model.User;

public interface UserDao {

	void saveUser(User user);

	List<Pets> home();

	List<Pets> myPetList(int petId);

	void savePet(Pets pets);

	User checkUser(String name, String passwd);

	void buyPet(int petId, int userId);

}
