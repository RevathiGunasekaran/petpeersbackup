package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.model.Pets;
import com.model.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userRepo;

	public void setUserRepo(UserDaoImpl userRepo) {
		this.userRepo = userRepo;
	}

	public UserServiceImpl(UserDao userRepo) {
		super();
		this.userRepo = userRepo;
	}

	public void saveUser(User user) {

	}

	public List<Pets> home() {
		return null;
	}

	public List<Pets> myPetList(int petId) {
		return null;
	}

	public void savePet(Pets pets) {

	}

	public User checkUser(String name, String passwd) {
		return null;
	}

	public void buyPet(int petId, int userId) {

	}

}
