package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.dao.UserDao;
import com.model.Pets;
import com.model.User;

public interface UserService {
	
	void saveUser(User user);

	List<Pets> home();

	List<Pets> myPetList(int id);

	void savePet(Pets pets);

	User checkUser(String name, String passwd);

	void buyPet(int petId, int userId);

}
